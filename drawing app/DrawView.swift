//
//  DrawView.swift
//  drawing app
//
//  Created by urvashi khatuja on 3/31/17.
//  Copyright © 2017 urvashi khatuja. All rights reserved.
//

import UIKit

class DrawView: UIView {

    
    var lines: [Line] = []
    var lastPoint: CGPoint!
    
     init(coder aDecoder: NSCoder!)
    {
        super.init(coder: aDecoder)
        }
    
    
    override func touchesBegan(_ touches: NSSet!, with event: UIEvent!) {
        lastPoint = touches.anyObject().locationInView(self)
    }
    
    override func touchesMoved(_ touches: NSSet!, with event: UIEvent!) {
        var newpoint = touches.anyObject().locationInView(self)
        lines.append(Line(start: lastPoint, end:newpoint))
        lastPoint = newpoint
    }
    
    override func draw(_ rect: CGRect) {
        var context = UIGraphicsGetCurrentContext()
        CGContextBeginPath(context)
        for line in lines {
            let context : CGContext = CGContextMoveToPoint as! CGContext
            
        context.CGContextMoveToPoint (context, line.start.x, line.start.y)
        CGContextAddLineToPoint (context, line.end.x, line.end.y)
        }
        
    }
}
 
